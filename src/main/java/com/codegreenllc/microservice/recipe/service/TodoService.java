package com.codegreenllc.microservice.recipe.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codegreenllc.microservice.recipe.dto.TodoDto;
import com.codegreenllc.microservice.recipe.repository.TodoRepository;

@Transactional
@Service
public class TodoService {
	@Autowired
	TodoRepository todoRepository;

	public List<TodoDto> getAllTodos() {
		return StreamSupport.stream(todoRepository.findAll().spliterator(), false).map(g -> {
			// You should use a mapper like mapstruct here
			final TodoDto result = new TodoDto();
			result.setId(g.getId());
			result.setVersion(g.getVersion());
			result.setDescription(g.getDescription());
			result.setCompleted(g.isCompleted());
			return result;
		}).collect(Collectors.toList());
	}
}
