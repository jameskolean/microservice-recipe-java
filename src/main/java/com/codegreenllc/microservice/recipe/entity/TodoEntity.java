package com.codegreenllc.microservice.recipe.entity;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
public class TodoEntity extends Auditable<String> {
	private boolean completed;
	private String description;
}
