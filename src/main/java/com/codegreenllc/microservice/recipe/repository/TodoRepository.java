package com.codegreenllc.microservice.recipe.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.codegreenllc.microservice.recipe.entity.TodoEntity;

@Repository
public interface TodoRepository extends CrudRepository<TodoEntity, String> {
}
