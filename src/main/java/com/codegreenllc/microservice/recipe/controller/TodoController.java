package com.codegreenllc.microservice.recipe.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codegreenllc.microservice.recipe.dto.TodoDto;
import com.codegreenllc.microservice.recipe.service.TodoService;

@RestController
public class TodoController {
	@Autowired
	TodoService todoService;

	@RequestMapping("/v1/todo")
	public List<TodoDto> getAllTodos() {
		return todoService.getAllTodos();
	}

}
