package com.codegreenllc.microservice.recipe.config.jpa;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
// Get user from security context
		return Optional.of("System");
	}
}
