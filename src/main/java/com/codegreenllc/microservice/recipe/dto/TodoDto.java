package com.codegreenllc.microservice.recipe.dto;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TodoDto {
	private boolean completed;
	private String description;
	private UUID id;
	private Long version;
}
